package revisaoDAO;

import java.util.ArrayList;
import java.util.List;

import domain.Veiculo;

public class VeiculoDAO {
	
	static List <Veiculo> veiculos = new ArrayList<>(); 
	
	public static void salvarVeiculo(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static void listar() {
		for (int i = 0; i < veiculos.size(); i++) {
			System.out.println(veiculos.get(i));
		}
		
	}

	public static void listarInvertido() {
		for (int i =  veiculos.size(); i > 0; i--) {
			System.out.println(veiculos.get(i));
		}
				
	}

}
