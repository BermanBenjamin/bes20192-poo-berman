package revisaoTUI;

import domain.Caminhao;
import domain.Carro;
import domain.Moto;
import revisaoBO.VeiculosBO;

public class VeiculoTUI {

	public static void main(String[] args) {

		Carro carro = new Carro("ayguws2165459", 2001, 40000, 4);

		Caminhao caminhao = new Caminhao("ayguws2165455", 2001, 1200000, 8, null);

		Moto moto = new Moto("ayguws2165489", 1999, 50000, "hornet");

		VeiculosBO.IncluirCarro(carro);

		VeiculosBO.IncluirCaminhao(caminhao);

		VeiculosBO.IncluirMoto(moto);

		VeiculosBO.listar();

		VeiculosBO.listarInvertido();

	}

}
