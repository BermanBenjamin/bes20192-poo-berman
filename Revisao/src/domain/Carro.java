package domain;

public class Carro extends Veiculo {
	private Integer qtdPortas;

	@Override
	public String toString() {
		return "Carro [qtdPortas=" + qtdPortas + ", Placa =" + getPlaca() + ", Anodefabricação ="
				+ getAnodefabricação() + ", Valor =" + getValor() ;
	}

	public Carro(String placa, Integer anodefabricação, Integer valor, Integer qtdPortas) {
		super(placa, anodefabricação, valor);
		this.qtdPortas = qtdPortas;
	}

	public Integer getQtdPortas() {
		return qtdPortas;
	}

	public void setQtdPortas(Integer qtdPortas) {
		this.qtdPortas = qtdPortas;
	}
}
