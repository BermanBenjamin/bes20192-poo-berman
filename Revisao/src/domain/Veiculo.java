package domain;

public class Veiculo {
	private String placa;
	private Integer anodefabricação;
	private Integer valor;

	public Veiculo(String placa, Integer anodefabricação, Integer valor) {
		super();
		this.placa = placa;
		this.anodefabricação = anodefabricação;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnodefabricação() {
		return anodefabricação;
	}

	public void setAnodefabricação(Integer anodefabricação) {
		this.anodefabricação = anodefabricação;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

}
