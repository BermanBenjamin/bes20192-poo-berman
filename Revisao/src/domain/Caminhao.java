package domain;

public class Caminhao extends Veiculo {
	private Integer quantidadeDeEixos;
	private Enum<TipoDeCarga> TipoDeCarga;

	
	@Override
	public String toString() {
		return "Caminhao [quantidadeDeEixos=" + quantidadeDeEixos + ", TipoDeCarga=" + TipoDeCarga + ", Placa="
				+ getPlaca() + ", Anodefabricação=" + getAnodefabricação() + ", Valor =" + getValor()
				;
	}





	public Caminhao(String placa, Integer anodefabricação, Integer valor, Integer quantidadeDeEixos,
			Enum<domain.TipoDeCarga> tipoDeCarga) {
		super(placa, anodefabricação, valor);
		this.quantidadeDeEixos = quantidadeDeEixos;
		TipoDeCarga = tipoDeCarga;
	}





	public Integer getQuantidadeDeEixos() {
		return quantidadeDeEixos;
	}

	public void setQuantidadeDeEixos(Integer quantidadeDeEixos) {
		this.quantidadeDeEixos = quantidadeDeEixos;
	}

	public Enum<TipoDeCarga> getTipoDeCarga() {
		return TipoDeCarga;
	}


	public void setTipoDeCarga(Enum<TipoDeCarga> tipoDeCarga) {
		TipoDeCarga = tipoDeCarga;
	}


}
