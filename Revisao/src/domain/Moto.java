package domain;

public class Moto extends Veiculo {
	private String NomeDoModelo;


	

	public Moto(String placa, Integer anodefabricação, Integer valor, String nomeDoModelo) {
		super(placa, anodefabricação, valor);
		NomeDoModelo = nomeDoModelo;
	}



	@Override
	public String toString() {
		return "Moto [NomeDoModelo=" + NomeDoModelo + ", Placa=" + getPlaca() + ", Anodefabricação="
				+ getAnodefabricação() + ", Valor=" + getValor() ;
	}



	public String getNomeDoModelo() {
		return NomeDoModelo;
	}

	public void setNomeDoModelo(String nomeDoModelo) {
		NomeDoModelo = nomeDoModelo;
	}

}
