package domain;

public enum TipoDeCarga {
	
	L�QUIDO(1), GAS(2), SOLIDO(3);
	
	private int index;

	private TipoDeCarga(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

}
