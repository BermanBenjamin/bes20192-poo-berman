package revisaoBO;

import domain.Caminhao;
import domain.Carro;
import domain.Moto;
import revisaoDAO.VeiculoDAO;

public class VeiculosBO {

	public static void validarCarro(Carro carro) {
		if (carro.getValor() > 0 && carro.getAnodefabricação() > 0) {
			VeiculoDAO.salvarVeiculo(carro);
		}
	}

	public static void validarCaminhao(Caminhao caminhao) {

		if (caminhao.getValor() > 0 && caminhao.getAnodefabricação() > 0) {
			VeiculoDAO.salvarVeiculo(caminhao);
		}

	}

	public static void validarMoto(Moto moto) {
		if (moto.getValor() > 0 && moto.getAnodefabricação() > 0 && moto.getNomeDoModelo() != null) {
			VeiculoDAO.salvarVeiculo(moto);
		}
	}

	public static void IncluirMoto(Moto moto) {
		validarMoto(moto);
	}

	public static void IncluirCarro(Carro carro) {
		validarCarro(carro);
	}

	public static void IncluirCaminhao(Caminhao caminhao) {
		validarCaminhao(caminhao);

	}

	public static void listar() {
		VeiculoDAO.listar();

	}

	public static void listarInvertido() {
		// TODO Auto-generated method stub

		VeiculoDAO.listarInvertido();
	}

}
