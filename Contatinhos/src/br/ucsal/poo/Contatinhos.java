package br.ucsal.poo;

import java.util.Scanner;

public class Contatinhos {
	Scanner sc = new Scanner(System.in);

	String name;

	String telefone;

	Integer year;

	String data;

	Integer tipo;

	public String getName() {
		name = sc.next();
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefone() {
		telefone = sc.next();
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getYear() {
		year = sc.nextInt();
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getData() {
		data = sc.next();
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Integer getTipo() {
		tipo = sc.nextInt(2);
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

}
