package carro;

public class Veiculo {
	String placa;
	Integer anoDeFabricação, valor;
	

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoDeFabricação() {
		return anoDeFabricação;
	}

	public void setAnoDeFabricação(Integer anoDeFabricação) {
		this.anoDeFabricação = anoDeFabricação;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

}
