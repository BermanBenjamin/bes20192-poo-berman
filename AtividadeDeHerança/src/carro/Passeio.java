package carro;

public class Passeio extends Veiculo{
	Integer quantidadeDePassageiros, portaMalasCapacidade;

	@Override
	public String toString() {
		return "Tipo: Passeio \nQuantidade De Passageiros = " + quantidadeDePassageiros + "\nPorta Malas Capacidade = "
				+ portaMalasCapacidade + "\nPlaca = " + placa + "\nAno De Fabricação = " + anoDeFabricação + "\nValor="
				+ valor;
	}

	public Integer getQuantidadeDePassageiros() {
		return quantidadeDePassageiros;
	}

	public void setQuantidadeDePassageiros(Integer quantidadeDePassageiros) {
		this.quantidadeDePassageiros = quantidadeDePassageiros;
	}

	public Integer getPortaMalasCapacidade() {
		return portaMalasCapacidade;
	}

	public void setPortaMalasCapacidade(Integer portaMalasCapacidade) {
		this.portaMalasCapacidade = portaMalasCapacidade;
	}
}
