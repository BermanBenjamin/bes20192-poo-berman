package carro;

public class Moto extends Veiculo {
	Integer cilindradas;
	String categoria;

	
	
	@Override
	public String toString() {
		return "Tipo = Moto \nCilindradas = " + cilindradas + "\nCategoria = " + categoria + "\nPlaca = " + placa
				+ "\nAno De Fabricação = " + anoDeFabricação + "\nValor = " + valor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getCilindradas() {
		return cilindradas;
	}

	public void setCilindradas(Integer cilindradas) {
		this.cilindradas = cilindradas;
	}

}
