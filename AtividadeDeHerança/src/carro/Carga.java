package carro;

public class Carga extends Veiculo{
	Integer capacidadeDeCarga, quantidadeDeEixos, capacidadeDoTanque;

	public Integer getCapacidadeDeCarga() {
		return capacidadeDeCarga;
	}

	public void setCapacidadeDeCarga(Integer capacidadeDeCarga) {
		this.capacidadeDeCarga = capacidadeDeCarga;
	}

	public Integer getQuantidadeDeEixos() {
		return quantidadeDeEixos;
	}

	public void setQuantidadeDeEixos(Integer quantidadeDeEixos) {
		this.quantidadeDeEixos = quantidadeDeEixos;
	}

	public Integer getCapacidadeDoTanque() {
		return capacidadeDoTanque;
	}

	public void setCapacidadeDoTanque(Integer capacidadeDoTanque) {
		this.capacidadeDoTanque = capacidadeDoTanque;
	}

	@Override
	public String toString() {
		return "Tipo : Carga \nCapacidade De Carga = " + capacidadeDeCarga + "\nQuantidade De Eixos = " + quantidadeDeEixos
				+ "\nCapacidade Do Tanque = " + capacidadeDoTanque + "\nPlaca = " + placa + "\nAno De Fabricação = " 
				+ anoDeFabricação + "\nValor = " + valor ;
	}
}