package carro;

import java.util.Scanner;

public class TesteDeHerança {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Qual o tipo de veiculo que você tem ? \n1)Moto \n2)Passeio \n3)Carga");
		int tipo = sc.nextInt();
		if (tipo == 1) {
			PegarMoto(CriarMoto());
		} else if (tipo == 2) {
			PegarCarroDePasseio(CriarCarroDePasseio());
		} else if (tipo == 3) {
			PegarCarroDeCarga(CriarCarroDeCarga());
		} else {
			System.out.println("POR MAIS QUE EU QUEIRA... NÃO SE PODE COMPRAR UM FOGUETE");
		}
	}

	private static Object CriarCarroDeCarga() {
		Carga carga1 = new Carga();
		int dado;
		String dadoString;
		
		System.out.println("Ano de fabricação:");
		dado = sc.nextInt();
		carga1.setAnoDeFabricação(dado);
		System.out.println("Qual a placa ?");
		dadoString = sc.next();
		carga1.setPlaca(dadoString);
		System.out.println("Qual o valor ?");
		dado = sc.nextInt();
		carga1.setValor(dado);
		System.out.println("Qual a capacidade de carga ?");
		dado = sc.nextInt();
		carga1.setCapacidadeDeCarga(dado);
		System.out.println("Qual a capacidade do tanque ?");
		dado = sc.nextInt();
		carga1.setCapacidadeDoTanque(dado);
		System.out.println("Qual a quantidade de eixos do veiculo ?");
		dado = sc.nextInt();
		carga1.setQuantidadeDeEixos(dado);
		
		return carga1;
	}

	private static Object CriarCarroDePasseio() {
		Passeio passeio = new Passeio();

		int dado;
		String dadoString;
		
		System.out.println("Ano de fabricação:");
		dado = sc.nextInt();
		passeio.setAnoDeFabricação(dado);
		System.out.println("Qual a placa ?");
		dadoString = sc.next();
		passeio.setPlaca(dadoString);
		System.out.println("Qual o valor ?");
		dado = sc.nextInt();
		passeio.setValor(dado);
		System.out.println("Qual a capacidade do porta malas ?");
		dado = sc.nextInt();
		passeio.setPortaMalasCapacidade(dado);
		System.out.println("Qual a capacidade de passaigeiros suportada ?");
		dado = sc.nextInt();
		passeio.setQuantidadeDePassageiros(dado);
		
		return passeio;
	}

	private static Moto CriarMoto() {
		int dado;
		String dadoString;
		Moto moto1 = new Moto();
		System.out.println("Ano de fabricação:");
		dado = sc.nextInt();
		moto1.setAnoDeFabricação(dado);
		System.out.println("Quantas cilindradas ?");
		dado = sc.nextInt();
		moto1.setCilindradas(dado);
		System.out.println("Qual a categoria ?");
		dadoString = sc.next();
		moto1.setCategoria(dadoString);
		System.out.println("Qual a placa ?");
		dadoString = sc.next();
		moto1.setPlaca(dadoString);
		System.out.println("Qual o valor ?");
		dado = sc.nextInt();
		moto1.setValor(dado);

		return moto1;
	}

	private static void PegarMoto(Object moto) {
		System.out.println(moto.toString());
	}

	private static void PegarCarroDeCarga(Object object) {
		System.out.println(object.toString());
	}

	private static void PegarCarroDePasseio(Object passeio) {
		System.out.println(passeio.toString());
	}

}
